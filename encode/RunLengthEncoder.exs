defmodule RunLengthEncoder do

	def encode(word) do

		result = Regex.scan ~r{[A-z]}, word
		result = List.flatten result
		IO.puts _encode(result, [], 1)
	end

	defp _encode([], resultword, _currentlettercount) do
		resultword
	end
	
	defp _encode([head], resultword, currentlettercount) do
		resultword = resultword ++ [to_string(currentlettercount), head]
		_encode([], resultword, 0)
	end

	defp _encode([head, next | tail], resultword, currentlettercount)  when head == next do
		_encode([next | tail], resultword, currentlettercount + 1)	
	end
	
	defp _encode([head, next | tail], resultword, currentlettercount) when head != next do
		resultword = resultword ++ [to_string(currentlettercount), head]
		_encode([next | tail], resultword, 1)
	end

end

RunLengthEncoder.encode "JJJTTWPPMMMMYYYYYYYYYVVVVVV"
RunLengthEncoder.encode "AABBC"
RunLengthEncoder.encode "AABBCC"
